public class App {
    public static void main(String[] args) throws Exception {
        Person person = new Person("100", "Eduardo", "Medina");
        Shape box = new Box();

        System.out.println("Hello, World!");
        System.out.println("Person : "+person);
        System.out.println("box name: "+box.name());
        System.out.println("box area: "+box.area());
    }
}

public class Box implements Shape {

    private int side = 10;
    @Override
    public String name() {
        return "Box";
    }

    @Override
    public int area() {
        return side*side;
    }
    
}

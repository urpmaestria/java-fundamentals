public interface Shape {
    
    String name();
    int area();
}
